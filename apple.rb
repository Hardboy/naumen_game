module Apple
  def print_apple
    @apple_position_x = Random.rand(@width) if @apple_position_x.nil?
    @apple_position_y = Random.rand(@height) if @apple_position_y.nil?
    while @player.include? [@apple_position_y, @apple_position_x]
      print_apple
      return
    end
    @array_field[@apple_position_y][@apple_position_x] = -1
  end
end
