module Field
  def print_field
    print "\n"
    puts ' ' * 5 + '*' * 70

    @array_field.each_index do |i|
      print ' ' * 5 + '*'
      @array_field[i].each_index do |j|
        case @array_field[i][j]
        when -1
          print 'Q'
        when 0...@width * @height
          print 'o'
        else
          print ' '
        end
      end

      print "*\n"
    end

    puts ' ' * 5 + '*' * 70
  end
end
