require 'io/console'
require_relative 'player'
require_relative 'game_logic'
require_relative 'apple'
require_relative 'system_methods'
require_relative 'field'
require_relative 'config'

include Player
include GameLogic
include Apple
include SystemMethods
include Field
