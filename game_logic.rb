module GameLogic
  def menu
    cls
    file_read('main_page.txt')
    case STDIN.getch.downcase
    when 'r'
      level
    when 'q'
      exit
    else
      menu
    end
  end

  def level
    loop do
      case @last_key
      when 'd'
        player_walk(:rigth)
      when 'a'
        player_walk(:left)
      when 's'
        player_walk(:down)
      when 'w'
        player_walk(:up)
      else
        print "Send key 'w' or 'a' or 's' or 'd'"
      end

      game_over?
      input_key
      print_player_on_field
      print_apple
      print_field
      sleep @speed
      cls
    end
  end

  def restart_level
    @player = [[12, 2], [12, 1], [12, 0]]
    @last_key = ''
    @speed = 0.5
  end

  def game_over?
    if @player[0][0] > @height || @player[0][0] < 0 ||
        @player[0][1] > @width - 1 || @player[0][1] < 0
      game_over
    end
  end

  def game_over
    cls
    file_read('game_over.txt')
    key = STDIN.getch.downcase
    case key
    when 'r'
      restart_level
    when 'q'
      exit
    else
      game_over
    end
  end
end
