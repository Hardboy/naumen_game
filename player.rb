module Player
  def player_walk(route)
    case route
    when :up
      new_head = [@player[0][0] - 1, @player[0][1]]
    when :down
      new_head = [@player[0][0] + 1, @player[0][1]]
    when :left
      new_head = [@player[0][0], @player[0][1] - 1]
    when :rigth
      new_head = [@player[0][0], @player[0][1] + 1]
    else
      print('How did you do this?')
    end
    game_over if @player.include? new_head

    @player.unshift(new_head)

    if new_head == [@apple_position_y, @apple_position_x]
      @apple_position_y, @apple_position_x = nil
      if (@player.size % 10).zero?
        @speed -= 0.05 if @speed >= 0.1
      end
    else
      @player.delete_at(-1)
    end
  end

  def walk_back?(key)
    case key
    when 'd'
      return true if @last_key == 'a'
    when 'a'
      return true if @last_key == 'd' || @last_key == ''
    when 's'
      return true if @last_key == 'w'
    when 'w'
      return true if @last_key == 's'
    else
      false
    end
  end

  def print_player_on_field
    @player.each_index do |x|
      @array_field[@player[x][0]][@player[x][1]] = x
    end
  end
end
