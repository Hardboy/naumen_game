module SystemMethods
  def cls
    Gem.win_platform? ? (system 'cls') : (system 'clear')
    @array_field = Array.new(@height + 1).map! { Array.new(@width) }
  end

  def file_read(file)
    f = File.open(file)
    print f.read
    f.close
  end

  def input_key
    system('stty raw -echo')
    key = STDIN.read_nonblock(1) rescue nil
    system('stty -raw echo')
    if /[awsd]/.match(key) && @last_key != key && !walk_back?(key)
      @last_key = key
    end
  end
end
